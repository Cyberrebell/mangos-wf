#!/bin/bash

function init_database() {
    SQL_DUMP='CREATE DATABASE `'${1}'` /*!40100 COLLATE utf8mb4_general_ci */;'$'\n'
    SQL_DUMP=${SQL_DUMP}'USE `'${1}'`;'$'\n'
    SQL_DUMP=${SQL_DUMP}$(cat /opt/sql/${1}.sql)

    echo "${SQL_DUMP}" | docker_process_sql
}

init_database 'realmd'
init_database 'mangos'
init_database 'characters'

SQL_DUMP='USE `'realmd'`;'$'\n'
SQL_DUMP=${SQL_DUMP}$(cat /opt/sql/sql_account_extends.sql)$'\n'
SQL_DUMP=${SQL_DUMP}$(cat /opt/sql/sql_newtables.sql)
echo "${SQL_DUMP}" | docker_process_sql