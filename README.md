# mangos-wf
This repo is about to collect different versions of mangos-wf and make them work using docker.

**Warning: It is outdated and insecure. Don't use it for a real project!**

## How to run

`docker-compose up -d`

open http://127.0.0.1/

### Mangos Web FrontEnd (mangos-wf)
Was THE web cms for mangos based WoW private servers in the vanilla and TBC years.

© Sasha aka LordSc. lordsc@yandex.ru

© bestuk (not sure who he is. But he added some commits after lordsc)

© TGM, Peec, Nafe (not sure if they worked on a non-enhanced version I don't have. But they are in the author docs of mangosweb-enhanced so I guess they continued mangos-wf on assembla)

### Mangos Web Enhanced (mangosweb-enhanced)
Continued version for WotLk+

© Wilson212, Ionstorm66

### Branches
* master -> latest TBC version (mangos-wf)
* dev -> TBC rewrite (mangos-wf)
* wotlk -> latest WotLk version (mangosweb-enhanced)

### Sources
#### mangos-wf
v3: https://sourceforge.net/projects/mangos-wf-v3/

v4?: http://tools.assembla.com/mangosweb/ (deleted. Lost forever if nobody has a backup?) 

#### mangosweb-enhanced
v1: https://sourceforge.net/projects/mw-enhanced/ (oldest version found)

v1: https://code.google.com/archive/p/mwenhanced/downloads (FinalRelease v1 rev57)

v2: https://code.google.com/archive/p/mwenhanced/

v3: https://code.google.com/archive/p/mangoswebv3/

# Found an old version I don't have? Please open an issue. I want them all :)