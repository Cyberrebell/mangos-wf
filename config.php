<?php
$config = array(
'template'       => 'offlike',
'default_lang'   => 'en',        // Default lang // Don't change
'lang'           => 'en',        // Site Default lang
'db_type'        => 'mysql',
'db_host'        => 'sql',
'db_port'        => '3306',
'db_username'    => 'root',
'db_password'    => 'mangosweb',
'db_name'        => 'realmd',
'db_encoding'    => 'utf8'     // don't change
);

$news_forum_id = 1;            // forum id for "news"
$bugs_forum_id = 2;            // forum id for "bugtracker"

$templates = array('offlike');

?>