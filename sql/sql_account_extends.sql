DROP TABLE IF EXISTS `account_extend`;
DROP TABLE IF EXISTS `account_groups`;


CREATE TABLE `account_extend` (
  `account_id` int(11) unsigned NOT NULL,
  `g_id` smallint(5) unsigned NOT NULL DEFAULT '2',
  `avatar` varchar(60) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `homepage` varchar(100) DEFAULT NULL,
  `icq` varchar(12) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `signature` text,
  `hideemail` tinyint(1) NOT NULL DEFAULT '1',
  `hideprofile` tinyint(1) NOT NULL DEFAULT '0',
  `theme` smallint(5) unsigned NOT NULL DEFAULT '0',
  `forum_posts` int(10) unsigned NOT NULL DEFAULT '0',
  `registration_ip` varchar(15) NOT NULL DEFAULT '0.0.0.0',
  `activation_code` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


INSERT INTO `account_extend` (`account_id`) SELECT account.id FROM `account`;

UPDATE `account_extend` SET `g_id` = 4 WHERE `account_id` = (SELECT id FROM `account` WHERE `username` = 'ADMINISTRATOR');

CREATE TABLE `account_groups` (
  `g_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `g_title` varchar(32) NOT NULL,
  `g_prefix` char(6) DEFAULT NULL,
  `g_is_admin` tinyint(1) DEFAULT '0',
  `g_is_supadmin` tinyint(1) DEFAULT '0',
  `g_use_search` tinyint(1) DEFAULT '0',
  `g_view_profile` tinyint(1) DEFAULT '0',
  `g_post_new_topics` tinyint(1) DEFAULT '0',
  `g_reply_other_topics` tinyint(1) DEFAULT '0',
  `g_use_attach` tinyint(1) DEFAULT '0',
  `g_edit_own_posts` tinyint(1) DEFAULT '0',
  `g_delete_own_posts` tinyint(1) DEFAULT '0',
  `g_delete_own_topics` tinyint(1) DEFAULT '0',
  `g_forum_moderate` tinyint(1) NOT NULL DEFAULT '0',
  `g_use_pm` tinyint(1) DEFAULT '0',
  `g_gal_view` tinyint(1) NOT NULL DEFAULT '0',
  `g_gal_upload` tinyint(1) DEFAULT '0',
  `g_gal_download` tinyint(1) DEFAULT '0',
  `g_gal_moderate` tinyint(1) DEFAULT '0',
  `g_gal_balanceon` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`g_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


INSERT INTO `account_groups` (`g_id`, `g_title`, `g_prefix`, `g_is_admin`, `g_is_supadmin`, `g_use_search`, `g_view_profile`, `g_post_new_topics`, `g_reply_other_topics`, `g_edit_own_posts`, `g_delete_own_posts`, `g_delete_own_topics`, `g_use_attach`, `g_forum_moderate`, `g_use_pm`, `g_gal_view`, `g_gal_upload`, `g_gal_download`, `g_gal_moderate`, `g_gal_balanceon`) VALUES 
(1, 'Guests', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(2, 'Members', NULL, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1),
(3, 'Administrators', '+', 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(4, 'Root Admins', '&#165;', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(5, 'Banned', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

ALTER TABLE `account` ADD `I` varchar(40) NOT NULL DEFAULT '' AFTER `username`;

UPDATE `account` SET `I` = 'a34b29541b87b7e4823683ce6c7bf6ae68beaaac' WHERE `username` = 'ADMINISTRATOR';
UPDATE `account` SET `I` = '7841e21831d7c6bc0b57fbe7151eb82bd65ea1f9' WHERE `username` = 'GAMEMASTER';
UPDATE `account` SET `I` = 'a7f5fbff0b4eec2d6b6e78e38e8312e64d700008' WHERE `username` = 'MODERATOR';
UPDATE `account` SET `I` = '3ce8a96d17c5ae88a30681024e86279f1a38c041' WHERE `username` = 'PLAYER';
